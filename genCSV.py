import random

c = open('randomData.csv', 'w')

count = 1

while count <= 20:
    count += 1
    c.write(str(random.randint(60, 75)))
    c.write(',')
    c.write(str(random.randint(120, 300)))

    if count <= 20:
        c.write('\n')
    else:
        continue

c.close()
