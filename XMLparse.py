import csv

reader = csv.reader(open('randomData.csv', 'r'), delimiter=',')

x = open('data.xml', 'w')

x.write('<main>')

for row in reader:
    x.write('\n\t' + '<data>')
    x.write('\n\t\t' + '<data1>' + row[0] + '</data1>')
    x.write('\n\t\t' + '<data2>' + row[1] + '</data2>')
    x.write('\t\n' + '\t</data>')

x.write('\n</main>')
x.close()
