import csv

reader = csv.reader(open('randomData.csv', 'r'), delimiter=',')

js = open('data.json', 'w')

js.write('{\n \t"data1 (height [in]), data2 (weight [lbs])": [')

rows = 0

for row in reader:
    rows += 1
    js.write('\n\t{\n')
    js.write('\t\t"data1": ' + row[0] + ', \n')
    js.write('\t\t"data2": ' + row[1] + '\n')

    if rows < 20:
        js.write(' \t},')
    else:
        js.write(' \t}')

js.write('\n] }')
js.close()
